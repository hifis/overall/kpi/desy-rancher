FROM python:3.10-alpine
RUN apk add git
RUN git clone https://mhamming:rUKR9hTnZ-12am_93RbJ@gitlab.desy.de/moritz.hamminger/rancherkpis.git
WORKDIR "/rancherkpis"
RUN git remote set-url origin https://mhamming:rUKR9hTnZ-12am_93RbJ@gitlab.desy.de/moritz.hamminger/rancherkpis.git
RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt
CMD [ "python", "./main.py" ]
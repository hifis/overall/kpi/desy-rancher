# RancherKPIs

This Repo fetch the metadata from rancher/prometheus and store it in the csv file and push to desy prometheus instance


## Data
In the data folder you can find the csv file which contains the following data \
`{timestamp}, {user_count}, {project_count}, {volume_count}, {volume_size}, {pods_count}, {ingress_count}, {cpu}, {memory}`

All following data related to users and projects authenticated via [Helmholtz AAI](https://hifis.net/aai),
internally marked via label `org=hifis`.

| Field         | Meaning                                          | Unit           | Datasource  | Query |
|---------------|--------------------------------------------------|----------------|-------------|----|
| timestamp     | timestamp of the data                            | %d.%m.%Y %H:%M | -           | -  |
| user_count    | number of users in total                         | -              | rancher api | -  |
| project_count | number of projects in total                      | -              | rancher api | -  |
| volume_count  | number of volumes in total in projects           | -              | rancher api | -  |
| volume_size   | total size of the volumes in projects            | GiB            | rancher api | -  |
| pods_count    | number of pods in total in projects              | -              | rancher api | -  |
| ingress_count | number of ingress in total in projects           | -              | rancher api | -  |
| cpu           | total cpu usage in hours from pods in projects   | h              | prometheus  | last_over_time(container_cpu_usage_seconds_total{{pod="{pod.get("name")}", image=""}}[1d]) - last_over_time(container_cpu_usage_seconds_total{{pod="{pod.get("name")}", image=""}}[1d] offset 1d) |
| memory        | total memory usage in GB from pods in projects   | GB             | prometheus  | avg_over_time(container_memory_usage_bytes{{pod="{pod.get("name")}", image=""}}[1d]) |

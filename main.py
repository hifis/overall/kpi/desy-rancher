import datetime
import git
import requests
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import urllib.parse
from os import getenv

headers = {
    'Authorization': f'Bearer {getenv("token")}'
}

cluster_id = getenv("cluster_id")


def get_rancher_user_count() -> int:
    """
    connect to rancher api and gets the registered users
    :return: the length of the users array
    """
    projects = requests.get('https://rancher.desy.de/v3/users', headers=headers)
    return len(projects.json()['data'])


def get_filtered_projects() -> list:
    """
    connect to rancher api and gets the registered projects and filter them by the label 'org=hifis'
    :return: list of filtered projects
    """
    projects = requests.get(f'https://rancher.desy.de/v3/projects?clusterId={cluster_id}', headers=headers)
    filtered_projects = list(filter(lambda x: x['labels'].get('org') == 'hifis', projects.json()['data']))
    return filtered_projects


def get_rancher_volumes(filtered_projects: list) -> list:
    """
    connect to rancher api and gets the registered volumes and filter them by the label 'org=hifis'
    :return: list of filtered volumes
    """
    filtered_volumes = []

    for project in filtered_projects:
        volumes = requests.get(f'https://rancher.desy.de/v3/projects/{project["id"]}/persistentvolumeclaims',
                               headers=headers)
        filtered_volumes.extend(volumes.json()['data'])

    return filtered_volumes


def sum_volume_size(volumes: list) -> int:
    """
    calculate the sum of the volume size
    :param volumes: list of volumes
    :return: int sum of the volume size
    """
    return sum([int(volume['status']['capacity']['storage'].replace('Gi', '')) for volume in volumes])


def get_pods(filtered_projects: list) -> list:
    """
    connect to rancher api and gets the registered pods and filter them by the label 'org=hifis'
    :return: list of filtered pods
    """
    filtered_pods = []

    for project in filtered_projects:
        pods = requests.get(f'https://rancher.desy.de/v3/projects/{project["id"]}/pods', headers=headers)
        filtered_pods.extend(pods.json()['data'])

    return filtered_pods


def get_ingresses(filtered_projects: list) -> list:
    """
    connect to rancher api and gets the registered ingresses and filter them by the label 'org=hifis'
    :return: list of filtered ingresses
    """
    filtered_ingresses = []

    for project in filtered_projects:
        ingresses = requests.get(f'https://rancher.desy.de/v3/projects/{project["id"]}/ingresses', headers=headers)
        filtered_ingresses.extend(ingresses.json()['data'])

    return filtered_ingresses


def get_cpu_hours(pods: list) -> float:
    """
    get the cpu usage of the pods from prometheus
    :param pods:  list of pods
    :return:  float cpu usage in hours
    """
    seconds = 0
    for pod in pods:
        query = f'last_over_time(container_cpu_usage_seconds_total{{pod="{pod.get("name")}", image=""}}[1d]) - last_over_time(container_cpu_usage_seconds_total{{pod="{pod.get("name")}", image=""}}[1d] offset 1d)'
        print(query)
        url = f"http://it-k8s-ric-guestcluster2.desy.de/api/v1/query?query={urllib.parse.quote(query)}"
        response = requests.get(url)
        if response.ok:
            temp_data = response.json().get("data").get("result")
            print(temp_data)
            if len(temp_data) > 0:
                print("keks")
                seconds += float(temp_data[0].get("value")[1])

    seconds_to_hours = seconds / 60 / 60

    return seconds_to_hours


def get_memory(pods: list) -> float:
    """
    get the memory usage of the pods from prometheus
    :param pods: list of pods
    :return: float memory usage in GB
    """
    memory = 0
    for pod in pods:
        query = f'avg_over_time(container_memory_usage_bytes{{pod="{pod.get("name")}", image=""}}[1d])'
        url = f"http://it-k8s-ric-guestcluster2.desy.de/api/v1/query?query={urllib.parse.quote(query)}"
        response = requests.get(url)
        if response.ok:
            temp_data = response.json().get("data").get("result")
            if len(temp_data) > 0:
                memory += float(temp_data[0].get("value")[1])

    memory_to_gb = memory / 10 ** 9

    return memory_to_gb


def append_to_csv(user_count: int,
                  project_count: int,
                  volume_count: int,
                  volume_size: int,
                  pods_count: int,
                  ingress_count: int,
                  cpu: float,
                  memory: float) -> None:
    """
    add the param 'value' with timestamp to the csv file specified in the env 'filename'
    :param user_count: the number of users
    :param project_count: the number of projects
    :param volume_count: the number of volumes
    :param volume_size: the size of the volumes
    :param pods_count: the number of pods
    :param ingress_count: the number of ingresses
    :param cpu: the cpu usage
    :param memory: the memory usage
    :return: None
    """
    now = get_formatted_date()
    with open(getenv("filename"), "a") as f:
        f.write(f"{now},{user_count},{project_count},{volume_count},{volume_size},{pods_count},{ingress_count},{cpu},{memory}\n")


def get_formatted_date() -> str:
    """
    This function generate a formatted utc time string
    :return: str Date sting like "05.09.2022 10:00"
    """
    return datetime.datetime.utcnow().strftime("%d.%m.%Y %H:%M")


def send_to_3rd_party_metric(user_count: int,
                             project_count: int,
                             volume_count: int,
                             volume_size: int,
                             pods_count: int,
                             ingress_count: int,
                             cpu: float,
                             memory: float) -> None:
    """
    push the value to desy's 3rd party metric store
    :param user_count: the number of users
    :param project_count: the number of projects
    :param volume_count: the number of volumes
    :param volume_size: the size of the volumes
    :param pods_count: the number of pods
    :param ingress_count: the number of ingresses
    :param cpu: the cpu usage
    :param memory: the memory usage
    :return: None
    """
    registry = CollectorRegistry()
    user = Gauge('rancherkpi_user_count', 'count of user in rancher', registry=registry)
    user.set(user_count)
    projects = Gauge('rancherkpi_project_count', 'count of hifis projects in rancher', registry=registry)
    projects.set(project_count)
    volumes = Gauge('rancherkpi_volume_count', 'count of volumes in hifis projects in rancher', registry=registry)
    volumes.set(volume_count)
    volumes_size = Gauge('rancherkpi_volume_size', 'size of volumes in hifis projects in rancher', registry=registry)
    volumes_size.set(volume_size)
    pods = Gauge('rancherkpi_pod_count', 'count of pods in hifis projects in rancher', registry=registry)
    pods.set(pods_count)
    ingress = Gauge('rancherkpi_ingress_count', 'count of ingresses in hifis projects in rancher', registry=registry)
    ingress.set(ingress_count)
    cpu_value = Gauge('rancherkpi_cpu_hours', 'CPU Hours of pods in hifis projects in rancher', registry=registry)
    cpu_value.set(cpu)
    memory_value = Gauge('rancherkpi_used_memory', 'AVG Memory usage of pods in hifis projects in rancher',
                         registry=registry)
    memory_value.set(memory)
    push_to_gateway('it-p8s-exporter01.desy.de:9091', job='RancherKPI', registry=registry)


def main() -> None:
    # get the repo
    repo = git.Repo("/rancherkpis")
    o = repo.remotes.origin
    o.pull()

    # get the data from rancher and prometheus
    user_count = get_rancher_user_count()

    projects = get_filtered_projects()
    projects_count = len(projects)

    volumes = get_rancher_volumes(projects)
    volumes_count = len(volumes)
    volumes_total_size = sum_volume_size(volumes)

    pods = get_pods(projects)
    pods_count = len(pods)

    ingresses = get_ingresses(projects)
    ingresses_count = len(ingresses)

    cpu_hours = get_cpu_hours(pods)

    used_memory_gb = get_memory(pods)

    # store data
    append_to_csv(user_count, projects_count, volumes_count, volumes_total_size, pods_count, ingresses_count, cpu_hours,
                  used_memory_gb)
    send_to_3rd_party_metric(user_count, projects_count, volumes_count, volumes_total_size, pods_count, ingresses_count,
                             cpu_hours, used_memory_gb)
    # commit and push
    repo.git.add(update=True)
    repo.index.commit(f'add data for {get_formatted_date()}')
    o.push()


if __name__ == '__main__':
    main()
